import java.util.Scanner;
import java.util.Arrays;

public class FinalProject{

    private static int step;
    private static int place;
    private static int length;
    private static String input;
    private static String[] languages;

    public static void main(String[]args){
        System.out.println("Hello! Welcome to our FINAL project.  The last one ever!");
        System.out.println("#weOut #seniors");

        Scanner scan = new Scanner(System.in);
        System.out.println("Please enter the input for our Turing Machine:");
        input = scan.nextLine();
        input = input + " ";

        place = 0;
        length = input.length();
        languages = new String[3];

        System.out.println("Turing Machine");
        state0();

    }


    public static void state0(){
        //see if we are at end of the string, then check if 1 or 0
        tapeState(input.charAt(place));
        if (input.charAt(place)==' '){
            state2();
        }
        else if (input.charAt(place) == '1'){
            state1();
        }
        else if(input.charAt(place)=='0'){
            state6();
        }
        else{
            crash();
        }
    }
    public static void state1(){
        place++;
        tapeState(input.charAt(place));
        if(input.charAt(place) == '1'|| input.charAt(place)=='0'){
            languages[0] = "The language {ww | w e {0,1}} is accepted.";
            place++;
            state0();
        }
        else if(input.charAt(place) == '#'){
            state3();
        }
        else{
            crash();
        }
    }

    public static void state2(){
        System.out.println(Arrays.toString(languages));
        System.out.println("Congrats, you have reached the final state.");
        System.out.println("Visit again soon! \n Sincerely, Alan Turing <3");
    }
    public static void state3(){
        place++;
        tapeState(input.charAt(place));
        if (input.charAt(place)=='0'||input.charAt(place) == '1'){
            tapeState(input.charAt(place));
            state4();
        }
        else{
            crash();
        }
    }
    public static void state4(){
        place++;
        tapeState(input.charAt(place));
        if(input.charAt(place) == '#'){
            tapeState('#');
            state5();
        }
        else{
            crash();
        }
    }


    public static void state5(){
        place++;
        tapeState(input.charAt(place));
        if(input.charAt(place)== '0'|| input.charAt(place)=='1'){
            languages[1] = "The language {w#w#w | w e {0,1}} is accepted.";
            place++;
            state0();
        }
        else{
            crash();
        }
    }
    public static void state6(){
        place++;
        tapeState(input.charAt(place));
        if(input.charAt(place)=='1'){
            languages[0] = "The language {ww | w e {0,1}} is accepted.";
            place++;
            state0();
        }
        else if(input.charAt(place)=='0'){
            state7();
        }
        else if(input.charAt(place)=='#'){
            state3();
        }
        else{
            crash();
        }
    }
    public static void state7(){
        place++;
        tapeState(input.charAt(place));
        if(input.charAt(place) == '0'){
            state6();
        }
        else if(input.charAt(place) == ' '){
            languages[2] = "The language w | w contains an even number of 0s and w e {0}*} is accepted.";
            state2();
        }
        else{
            crash();
        }
    }

    public static void tapeState(char read){
        System.out.println("Step: "+place);
        System.out.println("Current Configuration of machine: ");
        String head = "";
        for (int i = 0; i<= input.length(); i++){
            if (i != place){
            head = head + " ";
            }
            else if (i == place){
                head = head + "H";
            }
        }
        System.out.println("*****************************");
        System.out.println(head);
        System.out.println(input);
        System.out.println("*****************************");
        System.out.println("\tCharacter read: "+ read +"\n\tCharacter written: "+read+"\n\tMovement direction: R");

    }

    public static void crash(){
        System.out.println("Machine CRRRASSSHHHHEDDDDD!");
    }

}
