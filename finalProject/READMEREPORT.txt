SJ Guillaume & Claire Pickhardt
Professor Bonham-Carter
Computer Science 230
9 December 2016

Initially, when we started this project, we were concerned because we wanted to work together but we had no experience in creating a GUI. Once we were informed by Professor Bonham-Carter that
we no longer needed a GUI, the project's end seemed to be within our reach. Originally, we were going to code this program in the Python language. However, we decided that we would rather use
Java to do our project because it would keep us more organized and both of us have more experience writing in Java.

Our program is contained within a single file but has many methods, specifically one for each state that the machine traverses through. When the program is run, it asks the user in the terminal
for an input string containing zeroes and ones, as most Turing machines do. It initializes the step counter and place counter to zero, and each time the state methods are hit, both of the
counters are incremented up one number. From here on out, the code in the states does all the work. Each state outputs what number they are for the user's convenience, and successfully
moves to another state depending on the input string until finally coming to the end of the input and successfully printing out the results in the terminal for the user.

We were motivated to do this project because we have been learning about Turing machines all semester and we were enamored by them! Also, Claire really loves the movie "The Imitation Game"
so any way to research the work of the late Alan Turing is of interest to her.

Overall, this has been a great class and we have learned quite a lot, so thank you!
